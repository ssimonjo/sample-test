
from src.Random import Random
from src.Operation import Operation

class Happiness():
    def __init__(self, sign):
        self.sign = sign

    def am_i_happy(self):
        a = Random().get_random_value()
        op = Operation(a, 5)

        result = op.sum()

        if result >= 7:
            return "happy"
        elif result < 7:
            return "not happy"
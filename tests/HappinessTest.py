import unittest
from src import Happiness
from unittest.mock import patch, MagicMock

class HappinessFunctionality(unittest.TestCase):

    @patch("src.Random.Random.get_random_value")
    def test_i_am_happy_positive(self, op_mock):
        op_mock.return_value = 5
        result = Happiness.Happiness("positive")
        self.assertEqual(result.am_i_happy(), "happy")

if __name__ == '__main__':
    unittest.main()